#!/usr/bin/env bats

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "the image has alpine os installed" {
  run docker run --rm ${container} awk -F= '$1=="ID" { print $2 ;}' /etc/os-release

  assert_success
  assert_output "alpine"
}

@test "the image has nginx installed" {
  run docker run --rm --entrypoint=/bin/sh ${container} -c '[ -x /usr/sbin/nginx ]'

  assert_success
}

@test "the image has curl installed" {
  run docker run --rm --entrypoint=/bin/sh ${container} -c '[ -x /usr/bin/curl ]'

  assert_success
}

@test "the image has git installed" {
  run docker run --rm --entrypoint=/bin/sh ${container} -c '[ -x /usr/bin/git ]'

  assert_success
}
